ARG MYSQL_VERSION="8.0.22"
FROM mysql:$MYSQL_VERSION

ENV MYSQL_ROOT_PASSWORD "root"
ENV MYSQL_DATABASE=shopware
ENV MYSQL_USER=shopware
ENV MYSQL_PASSWORD=shopware


RUN apt update \
    && apt install -y ssh less openssh-client

COPY bin/ /usr/local/bin
RUN chmod -R 755 /usr/local/bin

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
