## Docker Container for easily dumping database through an ssh tunnel


### Execution examples
```
  docker run -eSSH_KEY="$(cat ~/.ssh/id_rsa)" -v$(pwd)/.my.cnf:/root/.my.cnf -e MYSQL_DATABASE=mysqldata -v$(pwd)/dumps/:/dump/ -eSSH_USER=sshuser -eSSH_HOST=example.com --rm forestsoft_mysqldumper shopware6
```