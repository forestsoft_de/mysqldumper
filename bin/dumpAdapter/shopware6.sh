#!/bin/bash
set -e -o pipefail
chmod 0444 ~/.my.cnf

HOST=${HOST:-"127.0.0.1"}
MYSQL_DATABASE=${MYSQL_DATABASE:-"shopware"}
DUMP_DIR=${DUMP_DIR:-"/dump"}

EXCLUDED_TABLES=(
  import_export_log
  cart
  customer
  customer_address
  log_entry
  product_search_keyword
  product_keyword_dictionary
  product_review
  import_export_file
  order
  order_customer
  order_address
  order_delivery
  order_tag
  order_transaction
  order_line_item
  order_delivery_position
)

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"; do
  :
  IGNORED_TABLES_STRING+=" --ignore-table=${MYSQL_DATABASE}.${TABLE}"
done

echo "Test connection"
mysql -h ${HOST} ${MYSQL_DATABASE} -e "select count(*) from sales_channel_domain"
echo "Dump structure of ${MYSQL_DATABASE} on Host ${HOST} to ${DUMP_DIR}/10-schema_latest.sql.gz"
mysqldump --no-tablespaces --no-data --no-create-db -h ${HOST} ${MYSQL_DATABASE} | gzip >${DUMP_DIR}/10-schema_latest.sql.gz
echo "Dump data of ${MYSQL_DATABASE} on Host ${HOST} to ${DUMP_DIR}/15-data_latest.sql.gz"
mysqldump --no-tablespaces --no-create-info --skip-triggers --no-create-db -h ${HOST} ${MYSQL_DATABASE} ${IGNORED_TABLES_STRING} | gzip >${DUMP_DIR}/15-data_latest.sql.gz
echo "Dump test data of ${MYSQL_DATABASE} on Host ${HOST} to ${DUMP_DIR}/20-testdata_latest.sql.gz"
mysqldump --no-tablespaces --no-create-info --skip-triggers --no-create-db -h ${HOST} ${MYSQL_DATABASE} \
  plugin \
  plugin_translation \
  locale \
  locale_translation \
  migration \
  language \
  media_folder \
  media_default_folder \
  media_folder_configuration \
  media_folder_configuration_media_thumbnail_size \
  sales_channel \
  sales_channel_api_context \
  sales_channel_country \
  sales_channel_currency \
  sales_channel_domain \
  sales_channel_language \
  sales_channel_shipping_method \
  sales_channel_payment_method \
  sales_channel_type \
  sales_channel_type_translation \
  theme \
  theme_media \
  theme_sales_channel \
  theme_translation \
  tax \
  tax_rule \
  tax_rule_type \
  tax_rule_type_translation \
  system_config |
  gzip >${DUMP_DIR}/20-testdata_latest.sql.gz
