#!/bin/bash

if [ "$1" == "" ]; then
    echo "Please define dump adapter like shopware"
    exit 255
fi

. /usr/local/bin/sshcon.sh
. /usr/local/bin/mysqlcon.sh

if [ -x "/usr/local/bin/dumpAdapter/${1}.sh" ]; then
  . /usr/local/bin/dumpAdapter/${1}.sh
else 
  echo ""
  echo "Dump adapter /usr/local/bin/dumpAdapter/${1}.sh not found"
  echo "Available adapters:"
  ls /usr/local/bin/dumpAdapter/ | less
  exit 255
fi