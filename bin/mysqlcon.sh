#!/bin/bash

if [ ! -f "/root/.my.cnf" ]; then
    echo "[client]" > /root/.my.cnf
    echo "user=${MYSQL_USER}" >> /root/.my.cnf
    echo "password=${MYSQL_PASSWORD}" >> /root/.my.cnf
fi
