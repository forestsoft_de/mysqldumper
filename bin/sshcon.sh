#!/bin/bash

if [ "${SSH_HOST-:""}" == "" ]; then
   echo "please define environment variable SSH_HOST"
   exit 255
fi

if [ "${SSH_KEY-:""}" == "" ]; then
   echo "please define environment variable SSH_KEY"
   exit 255
fi

if [ "${SSH_USER-:""}" == "" ]; then
   echo "please define environment variable SSH_USER"
   exit 255
fi

if [ ! -d $HOME/.ssh/ ] && [ "$SSH_KEY" != "" ]; then
  command -v ssh-agent >/dev/null
  echo "Add SSH Key to agent"
  eval $(ssh-agent -s)
  mkdir -p $HOME/.ssh/
  chmod 700 $HOME/.ssh
  echo "$SSH_KEY" | ssh-add -
  REMOTE_KEY=`ssh-keyscan $SSH_HOST 2> /dev/null`
  echo $REMOTE_KEY >> $HOME/.ssh/known_hosts
fi


ssh -f -o ExitOnForwardFailure=yes -o AddressFamily=inet -L 3306:127.0.0.1:3306 $SSH_USER@$SSH_HOST sleep 10